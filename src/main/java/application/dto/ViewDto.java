package application.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class ViewDto {
    private String domain;
    private String dateBegin;
    private String dateEnd;
    private String browser;
    private String ip;
}
