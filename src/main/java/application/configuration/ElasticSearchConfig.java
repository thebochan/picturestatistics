package application.configuration;

import application.configuration.properties.PropertiesConfig;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ElasticSearchConfig {

    private PropertiesConfig propertiesConfig;

    ElasticSearchConfig (PropertiesConfig propertiesConfig){
        this.propertiesConfig = propertiesConfig;
    }

    @Bean
    public RestHighLevelClient client(){
        RestHighLevelClient client = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost(propertiesConfig.getElasticConn().getHost(), propertiesConfig.getElasticConn().getPort(), "http")));
        return client;
    }
}
