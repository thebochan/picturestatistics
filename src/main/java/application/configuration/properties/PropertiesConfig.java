package application.configuration.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties
public class PropertiesConfig {

    private ElasticConnectionProperties elasticConn;
    private RabbitConnectionProperties rabbitConnectionProperties;

    PropertiesConfig(ElasticConnectionProperties elasticConn, RabbitConnectionProperties rabbitConnectionProperties){
        this.elasticConn = elasticConn;
        this.rabbitConnectionProperties = rabbitConnectionProperties;
    }

    public ElasticConnectionProperties getElasticConn() {
        return elasticConn;
    }

    public void setElasticConn(ElasticConnectionProperties elasticConn) {
        this.elasticConn = elasticConn;
    }

    public RabbitConnectionProperties getRabbitConnectionProperties() {
        return rabbitConnectionProperties;
    }

    public void setRabbitConnectionProperties(RabbitConnectionProperties rabbitConnectionProperties) {
        this.rabbitConnectionProperties = rabbitConnectionProperties;
    }
}
