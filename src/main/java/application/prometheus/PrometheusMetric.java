package application.prometheus;

import io.micrometer.prometheus.PrometheusMeterRegistry;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class PrometheusMetric {

    private PrometheusMeterRegistry meterRegistry;

    public void incremetDomainCounter() {
        meterRegistry.counter("domain_counter").increment();
    }
    public void incrementDomainUrlCounter() {
        meterRegistry.counter("domain_url_counter").increment();
    }
    public void incremetndUniqueCounter() {
        meterRegistry.counter("unique_counter").increment();
    }
    public void incrementDomainDateFilterCounter() {
        meterRegistry.counter("filter_domain_date_counter").increment();
    }
    public void incrementDomainIpCounter() {
        meterRegistry.counter("filter_domain_ip_counter").increment();
    }
    public void incrementDomainBrowserCounter() {
        meterRegistry.counter("filter_domain_browser_counter").increment();
    }
    public void incrementDomainBrowserDateCounter() {
        meterRegistry.counter("filter_domain_browser_date_counter").increment();
    }
    public void incrementDomainBrowserIpCounter() {
        meterRegistry.counter("filter_domain_browser_ip_counter").increment();
    }
    public void incrementDomainBrowserDateIp() {
        meterRegistry.counter("filter_domain_browser_date_ip_counter").increment();
    }
    public void incrementNewVisitsCount() {
        meterRegistry.counter("new_visit_count").increment();
    }

}
