package application.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@AllArgsConstructor
@Data
public class Visit implements Serializable {

    private String domain;
    private String url;
    private String browser;
    private String ip;
    private String date;
}
