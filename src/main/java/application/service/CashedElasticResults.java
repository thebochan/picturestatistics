package application.service;

import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Service
public class CashedElasticResults {

    private CashedElasticResultsDao cashedElasticResultsDao;
    private ElasticIndexWorker elasticIndexWorker;

    public ConcurrentMap<String, Long> countByDomain;// = new ConcurrentHashMap<>();
    public ConcurrentMap<String, Long> countByDomainAndUrl;// = new ConcurrentHashMap<>();
    public ConcurrentMap<String, Long> countUniqueIp;// = new ConcurrentHashMap<>();

    CashedElasticResults(CashedElasticResultsDao cashedElasticResultsDao, ElasticIndexWorker elasticIndexWorker){
        this.cashedElasticResultsDao = cashedElasticResultsDao;
        this.elasticIndexWorker = elasticIndexWorker;
    }

    @PostConstruct
    public void fillInMemoryParams() throws IOException {
        elasticIndexWorker.prepareIndex();
        countByDomain = cashedElasticResultsDao.countByDomain();
        countByDomainAndUrl = cashedElasticResultsDao.countByDomainAndUrl();
        countUniqueIp = cashedElasticResultsDao.countUniqueIp();
    }
}
