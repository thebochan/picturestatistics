package application.service;

import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.settings.Settings;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class ElasticIndexWorker {

    private RestHighLevelClient client;

    public ElasticIndexWorker(RestHighLevelClient client) {
        this.client = client;
    }

    public void prepareIndex() throws IOException {
        GetIndexRequest request = new GetIndexRequest("visits");
        boolean exists = client.indices().exists(request, RequestOptions.DEFAULT);
        if (!exists){
            CreateIndexRequest createIndex = new CreateIndexRequest("visits");
            createIndex.settings(Settings.builder()
                    .put("index.number_of_shards", 1)
                    .put("index.number_of_replicas", 1));
            Map<String, Object> domain = new HashMap<>();
            domain.put("type", "keyword");
            Map<String, Object> browser = new HashMap<>();
            browser.put("type", "text");
            Map<String, Object> date = new HashMap<>();
            date.put("type", "date");
            Map<String, Object> ip = new HashMap<>();
            ip.put("type", "ip");
            Map<String, Object> url = new HashMap<>();
            url.put("type", "keyword");
            Map<String, Object> properties = new HashMap<>();
            properties.put("domain", domain);
            properties.put("browser", browser);
            properties.put("date", date);
            properties.put("ip", ip);
            properties.put("url", url);
            Map<String, Object> mapping = new HashMap<>();
            mapping.put("properties", properties);
            createIndex.mapping(mapping);
            CreateIndexResponse createIndexResponse = client.indices().create(createIndex, RequestOptions.DEFAULT);
        }
    }
}
