package application.service;

import application.model.Visit;
import application.prometheus.PrometheusMetric;
import io.micrometer.prometheus.PrometheusMeterRegistry;
import lombok.AllArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

@AllArgsConstructor
@EnableRabbit
@Service
public class RabbitService {

    private static Logger logger = LogManager.getLogger();

    private CashedElasticResults cashedElasticResults;
    private VisitService visitService;
    private PrometheusMetric prometheusMetric;
    private CashedElasticResultsDao cashedElasticResultsDao;

    @RabbitListener(queues = "newvisit")
    public void listen(Visit visit) throws IOException {
        String domain = visit.getDomain();
        String url = visit.getUrl();
        String domainAndUrl = domain + "|" + url;
        visitService.addNewVisit(visit);
        prometheusMetric.incrementNewVisitsCount();
        logger.info("New visit added. Info " + visit);
        cashedElasticResults.countByDomain.compute(domain, (a, b) -> b == null ? 1L : b + 1L);
        cashedElasticResults.countByDomainAndUrl.compute(domain, (a, b) -> b == null  ? 1L : b + 1L);
        cashedElasticResults.countUniqueIp = cashedElasticResultsDao.countUniqueIp();
    }
}
