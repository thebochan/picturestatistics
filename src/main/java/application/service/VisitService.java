package application.service;

import application.dto.ViewDto;
import application.model.Visit;
import application.prometheus.PrometheusMetric;
import io.micrometer.prometheus.PrometheusMeterRegistry;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.*;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class VisitService {

    private RestHighLevelClient client;
    private PrometheusMetric prometheusMetric;

    VisitService (RestHighLevelClient client, PrometheusMetric prometheusMetric){
        this.client = client;
        this.prometheusMetric = prometheusMetric;
    }

    public void addNewVisit(Visit visit){
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("domain", visit.getDomain());
        dataMap.put("url", visit.getUrl());
        dataMap.put("browser", visit.getBrowser());
        dataMap.put("ip", visit.getIp());
        dataMap.put("date", visit.getDate());
        IndexRequest indexRequest = new IndexRequest("visits")
                .source(dataMap);
        try {
            IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Visit> displayVisitsByFilters(ViewDto requestView) throws IOException {
        final Scroll scroll = new Scroll(TimeValue.timeValueMillis(50L));
        List<Visit> resultList = new ArrayList<>();
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder query = prepareQuery(requestView);
        sourceBuilder.query(query);
        sourceBuilder.from(0);
        sourceBuilder.size(10);
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("visits");
        searchRequest.source(sourceBuilder);
        searchRequest.scroll(scroll);
        SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
        SearchHit[] searchHits = searchResponse.getHits().getHits();
        String scrollId = searchResponse.getScrollId();
        for (SearchHit hit : searchHits){
            Map<String, Object> sourceAsMap = hit.getSourceAsMap();
            Visit visit = new Visit((String) sourceAsMap.get("domain"), (String) sourceAsMap.get("url"), (String) sourceAsMap.get("browser")
                    , (String) sourceAsMap.get("ip"), (String) sourceAsMap.get("date"));
            resultList.add(visit);
        }
        if (searchHits.length != 1) {
            while (searchHits != null && searchHits.length > 0) {
                SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
                scrollRequest.scroll(scroll);
                searchResponse = client.scroll(scrollRequest, RequestOptions.DEFAULT);
                scrollId = searchResponse.getScrollId();
                searchHits = searchResponse.getHits().getHits();
                for (SearchHit hit : searchHits){
                    Map<String, Object> sourceAsMap = hit.getSourceAsMap();
                    Visit visit = new Visit((String) sourceAsMap.get("domain"), (String) sourceAsMap.get("url"), (String) sourceAsMap.get("browser")
                            , (String) sourceAsMap.get("ip"), (String) sourceAsMap.get("date"));
                    resultList.add(visit);
                }
            }
        }

        ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
        clearScrollRequest.addScrollId(scrollId);
        ClearScrollResponse clearScrollResponse = client.clearScroll(clearScrollRequest, RequestOptions.DEFAULT);

        for (SearchHit hit : searchHits){
            Map<String, Object> sourceAsMap = hit.getSourceAsMap();
            Visit visit = new Visit((String) sourceAsMap.get("domain"), (String) sourceAsMap.get("url"), (String) sourceAsMap.get("browser")
                    , (String) sourceAsMap.get("ip"), (String) sourceAsMap.get("date"));
            resultList.add(visit);
        }
        return resultList;
    }

    private BoolQueryBuilder prepareQuery(ViewDto view){
        String domain = view.getDomain();
        String browser = view.getBrowser();
        String dateBegin = view.getDateBegin();
        String dateEnd = view.getDateEnd();
        String ip = view.getIp();
        BoolQueryBuilder result = null;
        if (browser== null){
            if (ip == null){
                result = QueryBuilders.boolQuery().filter(QueryBuilders.matchQuery("domain", domain))
                        .filter(QueryBuilders.rangeQuery("date").gte(dateBegin))
                        .filter(QueryBuilders.rangeQuery("date").lte(dateEnd));
                prometheusMetric.incrementDomainDateFilterCounter();
            } else if (dateBegin == null && dateEnd == null){
                result = QueryBuilders.boolQuery().filter(QueryBuilders.matchQuery("domain", domain))
                        .filter(QueryBuilders.matchQuery("ip", ip));
                prometheusMetric.incrementDomainIpCounter();
            }
        }
        else if (ip == null){
            if (dateEnd == null && dateBegin == null){
                result = QueryBuilders.boolQuery().filter(QueryBuilders.matchQuery("domain", domain))
                        .filter(QueryBuilders.matchQuery("browser", browser));
                prometheusMetric.incrementDomainBrowserCounter();
            } else {
                result = QueryBuilders.boolQuery().filter(QueryBuilders.matchQuery("domain", domain))
                        .filter(QueryBuilders.matchQuery("browser", browser))
                        .filter(QueryBuilders.rangeQuery("date").gte(dateBegin))
                        .filter(QueryBuilders.rangeQuery("date").lte(dateEnd));
                prometheusMetric.incrementDomainBrowserDateCounter();
            }
        }
        else if (dateBegin == null && dateEnd == null){
            result = QueryBuilders.boolQuery().filter(QueryBuilders.matchQuery("domain", domain))
                    .filter(QueryBuilders.matchQuery("browser", browser))
                    .filter(QueryBuilders.matchQuery("ip", ip));
            prometheusMetric.incrementDomainBrowserIpCounter();
        }
        else {
            result = QueryBuilders.boolQuery().filter(QueryBuilders.matchQuery("domain", domain))
                    .filter(QueryBuilders.rangeQuery("date").gte(dateBegin))
                    .filter(QueryBuilders.rangeQuery("date").lte(dateEnd))
                    .filter(QueryBuilders.matchQuery("browser", browser))
                    .filter(QueryBuilders.matchQuery("ip", ip));
            prometheusMetric.incrementDomainBrowserDateIp();
        }
        return result;
    }



}
