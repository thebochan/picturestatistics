package application.service;

import org.elasticsearch.action.search.*;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Service
public class CashedElasticResultsDao {

    private RestHighLevelClient client;

    CashedElasticResultsDao(RestHighLevelClient client){
        this.client = client;
    }

    public ConcurrentMap<String, Long> countByDomain() throws IOException {
        final Scroll scroll = new Scroll(TimeValue.timeValueMillis(50L));
        ConcurrentMap<String, Long> countByDomain = new ConcurrentHashMap<>(16, 0.9f, 1);

        TermsAggregationBuilder aggregation = AggregationBuilders.terms("by_domain")
                .field("domain");

        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.aggregation(aggregation);
        sourceBuilder.query(QueryBuilders.matchAllQuery());
        sourceBuilder.from(0);
        sourceBuilder.size(10);

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("visits");
        searchRequest.source(sourceBuilder);
        searchRequest.scroll(scroll);

        SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
        String scrollId = searchResponse.getScrollId();
        Aggregations aggregations = searchResponse.getAggregations();
        Terms domainAggregation = aggregations.get("by_domain");
        SearchHit[] searchHits = searchResponse.getHits().getHits();
        for (Terms.Bucket bucket: domainAggregation.getBuckets()){
            countByDomain.put(bucket.getKey().toString(), bucket.getDocCount());
        }
        if (searchHits.length != 1) {
            while (searchHits != null && searchHits.length > 0) {
                SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
                scrollRequest.scroll(scroll);
                searchResponse = client.scroll(scrollRequest, RequestOptions.DEFAULT);
                scrollId = searchResponse.getScrollId();
                for (Terms.Bucket bucket : domainAggregation.getBuckets()) {
                    countByDomain.put(bucket.getKey().toString(), bucket.getDocCount());
                }
                searchHits = searchResponse.getHits().getHits();
            }
        }

        ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
        clearScrollRequest.addScrollId(scrollId);
        ClearScrollResponse clearScrollResponse = client.clearScroll(clearScrollRequest, RequestOptions.DEFAULT);
        return countByDomain;
    }

    public ConcurrentMap<String, Long> countByDomainAndUrl() throws IOException {
        final Scroll scroll = new Scroll(TimeValue.timeValueMillis(50L));
        ConcurrentMap<String, Long> countByDomainAndUrl = new ConcurrentHashMap<>(16, 0.9f, 1);
        TermsAggregationBuilder aggregation = AggregationBuilders.terms("by_domain")
                .field("domain");
        aggregation.subAggregation(AggregationBuilders.terms("by_url")
                .field("url"));

        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.aggregation(aggregation);
        sourceBuilder.query(QueryBuilders.matchAllQuery());
        sourceBuilder.from(0);
        sourceBuilder.size(10);

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("visits");
        searchRequest.source(sourceBuilder);
        searchRequest.scroll(scroll);

        SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
        String scrollId = searchResponse.getScrollId();
        Aggregations aggregations = searchResponse.getAggregations();
        Terms domainAggregation = aggregations.get("by_domain");
        SearchHit[] searchHits = searchResponse.getHits().getHits();
        for (Terms.Bucket bucket: domainAggregation.getBuckets()){
            Terms urlAggregation = bucket.getAggregations().get("by_url");
            for (Terms.Bucket innerBucket : urlAggregation.getBuckets()){
                countByDomainAndUrl.put(bucket.getKey() + "|" + innerBucket.getKey(), innerBucket.getDocCount());
            }
        }

        while (searchHits != null && searchHits.length > 0) {
            SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
            scrollRequest.scroll(scroll);
            searchResponse = client.scroll(scrollRequest, RequestOptions.DEFAULT);
            scrollId = searchResponse.getScrollId();
            for (Terms.Bucket bucket: domainAggregation.getBuckets()){
                Terms urlAggregation = bucket.getAggregations().get("by_url");
                for (Terms.Bucket innerBucket : urlAggregation.getBuckets()){
                    countByDomainAndUrl.put(bucket.getKey() + "|" + innerBucket.getKey(), innerBucket.getDocCount());
                }
            }
            searchHits = searchResponse.getHits().getHits();
        }

        ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
        clearScrollRequest.addScrollId(scrollId);
        ClearScrollResponse clearScrollResponse = client.clearScroll(clearScrollRequest, RequestOptions.DEFAULT);
        return countByDomainAndUrl;
    }

    public ConcurrentMap<String, Long> countUniqueIp() throws IOException {
        final Scroll scroll = new Scroll(TimeValue.timeValueMillis(50L));
        ConcurrentMap<String, Long> countUniqueIp = new ConcurrentHashMap<>(16, 0.9f, 1);

        TermsAggregationBuilder aggregation = AggregationBuilders.terms("by_domain")
                .field("domain");
        aggregation.subAggregation(AggregationBuilders.terms("by_ip")
                .field("ip"));

        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.aggregation(aggregation);
        sourceBuilder.query(QueryBuilders.matchAllQuery());
        sourceBuilder.from(0);
        sourceBuilder.size(10);

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("visits");
        searchRequest.source(sourceBuilder);
        searchRequest.scroll(scroll);

        SearchResponse searchResponse = client.search(searchRequest, RequestOptions.DEFAULT);
        String scrollId = searchResponse.getScrollId();
        Aggregations aggregations = searchResponse.getAggregations();
        Terms domainAggregation = aggregations.get("by_domain");
        SearchHit[] searchHits = searchResponse.getHits().getHits();
        for (Terms.Bucket bucket: domainAggregation.getBuckets()){
            Terms urlAggregation = bucket.getAggregations().get("by_ip");
            countUniqueIp.put(bucket.getKey().toString(), Long.valueOf(urlAggregation.getBuckets().size()));
        }

        while (searchHits != null && searchHits.length > 0) {
            SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
            scrollRequest.scroll(scroll);
            searchResponse = client.scroll(scrollRequest, RequestOptions.DEFAULT);
            scrollId = searchResponse.getScrollId();
            for (Terms.Bucket bucket: domainAggregation.getBuckets()){
                Terms urlAggregation = bucket.getAggregations().get("by_ip");
                countUniqueIp.put(bucket.getKey().toString(), Long.valueOf(urlAggregation.getBuckets().size()));
            }
            searchHits = searchResponse.getHits().getHits();
        }

        ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
        clearScrollRequest.addScrollId(scrollId);
        ClearScrollResponse clearScrollResponse = client.clearScroll(clearScrollRequest, RequestOptions.DEFAULT);
        return countUniqueIp;
    }
}
