package application.controller;

import application.dto.ViewDto;
import application.model.Visit;
import application.prometheus.PrometheusMetric;
import application.service.CashedElasticResults;
import application.service.VisitService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@Controller
public class VisitController {

    private VisitService visitService;
    private CashedElasticResults cashedElasticResults;
    private PrometheusMetric prometheusMetric;

    VisitController(CashedElasticResults cashedElasticResults, VisitService visitService, PrometheusMetric prometheusMetric){
        this.cashedElasticResults = cashedElasticResults;
        this.visitService = visitService;
        this.prometheusMetric = prometheusMetric;
    }

    @GetMapping("/countbydomain")
    @ResponseBody
    public Map<String, Long> countByDomain(@RequestParam String domain){
        Map<String, Long> result = new HashMap<>();
        Long count = cashedElasticResults.countByDomain.getOrDefault(domain, 0L);
        result.put(domain, count);
        prometheusMetric.incremetDomainCounter();
        return result;
    }

    @GetMapping("/countbydomainurl")
    @ResponseBody
    public Map<String, Long> countByDomainUrl(@RequestParam String domain, @RequestParam String url){
        Long count = cashedElasticResults.countByDomainAndUrl.getOrDefault(domain + "|" + url, 0L);
        Map<String, Long> result = new HashMap<>();
        String domainUrl = domain + "|" + url;
        result.put(domainUrl, count);
        prometheusMetric.incrementDomainUrlCounter();
        return result;
    }

    @GetMapping("/countunique")
    @ResponseBody
    public Map<String, Long> countUniqueByDomain(@RequestParam String domain){
        Long count = cashedElasticResults.countUniqueIp.getOrDefault(domain, 0L);
        Map<String, Long> result = new HashMap<>();
        result.put(domain, count);
        prometheusMetric.incremetndUniqueCounter();
        return result;
    }

    @PostMapping("/addView")
    public void addView(Visit visit){
        visitService.addNewVisit(visit);
    }

    @PostMapping("/displayvisits")
    @ResponseBody
    public List<Visit> displayByFiltres(@RequestBody ViewDto viewDto) throws IOException {
        List<Visit> result = visitService.displayVisitsByFilters(viewDto);
        if (result.size() == 0) {
            Visit error = new Visit("not found", "not found", "not found", "not found", "not found");
            result.add(error);
        }
        return result;
    }
}
